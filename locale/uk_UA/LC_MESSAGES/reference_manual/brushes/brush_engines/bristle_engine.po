# Translation of docs_krita_org_reference_manual___brushes___brush_engines___bristle_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___bristle_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 16:13+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<generated>:1
msgid "Weighted saturation"
msgstr "Зважене насичення"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.3-1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:0
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.3-2.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:1
msgid "The Bristle Brush Engine manual page."
msgstr "Сторінка підручника щодо рушія щетинкових пензлів."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:18
msgid "Bristle Brush Engine"
msgstr "Рушій щетинкових пензлів"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Hairy Brush Engine"
msgstr "Рушій волосяних пензлів"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:13
msgid "Sumi-e"
msgstr "Сумі-е"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:21
msgid ".. image:: images/icons/bristlebrush.svg"
msgstr ".. image:: images/icons/bristlebrush.svg"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:22
msgid ""
"A brush intended to mimic real-life brushes by drawing the trails of their "
"lines or bristles."
msgstr ""
"Пензель, який призначено для імітації звичайних щетинкових пензлів шляхом "
"показу на мазках слідів від щетинок."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:25
msgid "Brush Tip"
msgstr "Кінчик пензля"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:27
msgid "Simply put:"
msgstr "Якщо простіше:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:29
msgid "The brush tip defines the areas with bristles in them."
msgstr "Кінчик пензля визначає області із щетинками."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:30
msgid ""
"Lower opacity areas have lower-opacity bristles. With this brush, this may "
"give the illusion that lower-opacity areas have fewer bristles."
msgstr ""
"На ділянках із низькою непрозорістю непрозорість щетинок також буде низькою. "
"Для цього пензля це може створити ілюзію того, що на ділянках із низькою "
"непрозорістю менше щетинок."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:31
msgid ""
"The :ref:`option_size` and :ref:`option_rotation` dynamics affect the brush "
"tip, not the bristles."
msgstr ""
"Динаміка :ref:`option_size` і :ref:`option_rotation` впливає на кінчик "
"пензля, а не не щетинки."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:33
msgid "You can:"
msgstr "Можете зробити так:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:35
msgid ""
"Use different shapes for different effects. Be aware that complex brush "
"shapes will draw more slowly though, while the effects aren't always visible "
"(since in the end, you're passing over an area with a certain number of "
"bristles)."
msgstr ""
"Скористатися різними формами для різних ефектів. Втім, слід мати на увазі, "
"що пензлі складних форм малюють набагато повільніше, а ефекти від їхнього "
"використання не завжди є очевидними (оскільки врешті-решт ви просто "
"покриваєте фарбою певну ділянку зображення за допомогою певної кількості "
"щетинок на пензлі)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:36
msgid ""
"To decrease bristle density, you can also just use an autobrush and decrease "
"the brush tip's density, or increase its randomness."
msgstr ""
"Щоб зменшити щільність щетинок, ви також можете просто скористатися "
"автопензлем і зменшити щільність кінчика пензля або збільшити його "
"випадковість."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:39
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:41
msgid "Bristle Options"
msgstr "Параметри щетинок"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:43
msgid "The core of this particular brush-engine."
msgstr "Ядро цього окремо взятого рушія пензлів."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:46
msgid ""
"Think of it as pressing down on a brush to make the bristles further apart."
msgstr ""
"Це можна уявляти як роботу звичайного пензля: коли ви натискаєте на пензель, "
"щетинки розходяться."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:48
msgid ""
"Larger values basically give you larger brushes and larger bristle spacing. "
"For example, a value of 4 will multiply your base brush size by 4, but the "
"bristles will be 4 times more spaced apart."
msgstr ""
"Більші значення, на базовому рівні, даватимуть більші пензлі і більші "
"інтервали між щетинками. Наприклад, значення 4 призведе до множення базового "
"розміру пензля на 4, але також збільшить у чотири рази порожнє місце між "
"щетинками."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:49
msgid ""
"Use smaller values if you want a \"dense\" brush, i.e. you don't want to see "
"so many bristles within the center."
msgstr ""
"Використовуйте менші значення, якщо вам потрібен «щільний» пензель, тобто "
"якщо ви не хочете бачити так багато щетинок у центрі."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid "Scale"
msgstr "Масштаб"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:50
msgid ""
"Negative values have the same effect as corresponding positive values: -1.00 "
"will look like 1.00, etc."
msgstr ""
"Від'ємні значення працюють так само, як і додатні: -1.00 дає такий самий "
"вигляд, як 1.00."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:53
msgid "Adds a jaggy look to the trailing lines."
msgstr "Надає зубчастого вигляду лініям щетинок."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:55
msgid "At 0.00, all the bristles basically remain completely parallel."
msgstr "При значенні 0.00 усі лінії щетинок лишаються повністю паралельними."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid "Random Offset"
msgstr "Випадковий відступ"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:56
msgid ""
"At other values, the bristles are offset randomly. Large values will "
"increase the brush size a bit because of the bristles spreading around, but "
"not by much."
msgstr ""
"При інших значення відстань між щетинками буде випадковою. Використання "
"великих значень дещо збільшує розміри пензля через розсіювання щетинок, але "
"не набагато."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:57
msgid "Negative values have the same effect as corresponding positive values."
msgstr "Від'ємні значення працюють так само, як і додатні."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:58
msgid "Shear"
msgstr "Зсув"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:59
msgid ""
"Shear introduces an angle to your brush, as though you're drawing with an "
"oval brush (or the side of a round brush)."
msgstr ""
"Зсув додає кут для вашого пензля так, наче ви малюєте овальним пензлем (або "
"нахиленим круглим пензлем)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid "Density"
msgstr "Щільність"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:61
msgid ""
"This controls the density of bristles. Scale takes a number of bristles and "
"expands or compresses them into a denser area, whereas density takes a fixed "
"area and determines the number of bristles in it. See the difference?"
msgstr ""
"Цей параметр керує щільністю щетинок. За значенням масштабу програма розсіює "
"щетинки або стискає їх у меншу ділянку, а за значенням щільності "
"використовує сталу площу ділянки, але визначає кількість щетинок у ній. "
"Бачите різницю?"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:64
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.2-1.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:66
msgid ""
"This one maps \"Scale\" to mouse speed, thus simulating pressure with a "
"graphics tablet!"
msgstr ""
"Цей пункт пов'язує «Масштаб» зі швидкістю руху вказівника миші, таким чином "
"імітуючи роботу графічного планшета!"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid "Mouse Pressure"
msgstr "Натиск миші"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:68
msgid ""
"Rather, it uses the \"distance between two events\" to determine scale. "
"Faster drawing, larger distances."
msgstr ""
"Для визначення масштабу використовується «відстань між двома подіями». Чим "
"швидше малювання, тим більшою буде відстань."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:69
msgid ""
"This doesn't influence the \"pressure\" input for anything else (size, "
"opacity, rotation etc.) so you still have to map those independently to "
"something else."
msgstr ""
"Це значення не впливає на обробку вхідного «тиску» для усіх інших параметрів "
"(розміру, непрозорості, обертання, тощо), тому вам доведеться пов'язати тиск "
"із цими параметрами окремо."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:70
msgid "Threshold"
msgstr "Поріг"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:71
msgid ""
"This is a tablet feature. When you turn this on, only bristles that are able "
"to \"touch the canvas\" will be painted."
msgstr ""
"Це можливість для планшетів. Якщо пункт позначено, буде намальовано лише "
"щетинки, які могли «торкнутися полотна»."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:72
msgid "Connect Hairs"
msgstr "З’єднати щетинки"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:73
msgid "The bristles get connected. See for yourself."
msgstr "З'єднування щетинок. Поекспериментуйте."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:74
msgid "Anti-Aliasing"
msgstr "Згладжування"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:75
msgid "This will decrease the jaggy-ness of the lines."
msgstr "Цей параметр зменшує зубчастість ліній."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid "Composite Bristles"
msgstr "Композиція щетинок"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:77
msgid ""
"This \"composes the bristle colors within one dab,\" but explains that the "
"effect is \"probably subtle.\""
msgstr ""
"«Компонує кольори щетинок у один мазок», але пояснити наслідки застосування "
"цього ефекту «ймовірно, складно»."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:80
msgid ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"
msgstr ".. image:: images/brushes/Krita-tutorial7-B.I.2-2.png"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:82
msgid "Ink Depletion"
msgstr "Вичерпання чорнила"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:84
msgid ""
"This simulated ink depletion over drawing time. The value dictates how long "
"it will take. The curve dictates the speed."
msgstr ""
"Імітує вичерпання чорнила під час малювання. Значення визначає, на скільки "
"може вистачити чорнила. Крива визначає швидкість вичерпання чорнила."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:86
msgid "Opacity"
msgstr "Непрозорість"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:87
msgid "The brush will go transparent to simulate ink-depletion."
msgstr ""
"Пензель буде перетворюватися на прозорий для імітації вичерпання чорнила."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:89
msgid "The brush will be desaturated to simulate ink-depletion."
msgstr "Пензель буде зненасичено для імітації вичерпання чорнила."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:91
msgid "Saturation"
msgstr "Насиченість"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:94
msgid ""
"The brush will pick up colors from other brushes. You don't need to have :"
"guilabel:`Ink depletion` checked to activate this option, you just have to "
"check :guilabel:`Soak ink`. What this does is cause the bristles of the "
"brush to take on the colors of the first area they touch. Since the Bristle "
"brush is made up of independent bristles, you can basically take on several "
"colors at the same time."
msgstr ""
"Пензель братиме кольори з інших пензлів. Вам не доведеться позначати пункт :"
"guilabel:`Вичерпання чорнила` для активації цього пункту, достатньо "
"позначити пункт :guilabel:`Всмоктати чорнило`. Використання цього пункту "
"призведе до того, що щетинки пензля всотуватимуть кольори першої ділянки, "
"якої вони торкнуться. Оскільки щетинковий пензель створено із незалежних "
"щетинок, ви, на базовому рівні, можете взяти з полотна декілька кольорів "
"одночасно."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:98
msgid ""
"It will only take colors in the unscaled area of the brush, so if you're "
"using a brush with 4.00 scale for example, it will only take the colors in "
"the 1/4 area closest to the center."
msgstr ""
"Кольори буде взяти лише з немасштабованої ділянки пензля, отже, якщо ви, "
"наприклад, використовуєте пензель із масштабом 4.00, кольори буде взято лише "
"із розташованої у центрі 1/4 площі ділянки пензля."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:99
msgid "When the source is transparent, the bristles take black color."
msgstr "Якщо джерело є прозорим, щетинки всмоктуватимуть чорний колір."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid "Soak Ink"
msgstr "Всмоктати чорнило"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:104
msgid ""
"Be aware that this feature is a bit buggy though. It's supposed to take the "
"color from the current layer, but some buggy behavior causes it to often use "
"the last layer you've painted on (with a non-Bristle brush?) as source. To "
"avoid these weird behaviors, stick to just one layer, or paint something on "
"the current active layer first with another brush (such as a Pixel brush)."
msgstr ""
"Втім, слід мати на увазі, що у поточній версії ця можливість не позбавлена "
"вад. Пензель мав би брати колір із поточного шару, але через вади часто бере "
"колір з останнього шару, на якому ви малювали (за допомогою пензля, "
"відмінного від щетинкового?). Щоб уникнути цієї проблеми, користуйтеся лише "
"одним шаром або намалюйте спочатку щось на поточному активному шарі іншим "
"пензлем (наприклад піксельним пензлем)."

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:107
msgid "Works by modifying the saturation with the following:"
msgstr "Працює за рахунок зміни насиченості за такими параметрами:"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:109
msgid "Pressure weight"
msgstr "Вага тиску"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:110
msgid "Bristle length weight"
msgstr "Вага відстані щетинками"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:111
msgid "Bristle ink amount weight"
msgstr "Вага частки чорнила для щетинок"

#: ../../reference_manual/brushes/brush_engines/bristle_engine.rst:112
msgid "Ink depletion curve weight"
msgstr "Вага кривої вичерпання чорнила"
