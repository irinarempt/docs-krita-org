msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-05 22:26\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___crop.pot\n"

#: ../../reference_manual/tools/crop.rst:1
msgid "Krita's crop tool reference."
msgstr ""

#: ../../reference_manual/tools/crop.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/crop.rst:11
msgid "Crop"
msgstr ""

#: ../../reference_manual/tools/crop.rst:11
msgid "Trim"
msgstr ""

#: ../../reference_manual/tools/crop.rst:16
msgid "Crop Tool"
msgstr "裁切工具"

#: ../../reference_manual/tools/crop.rst:18
msgid ""
"The crop tool can be used to crop an image or layer. To get started, choose "
"the :guilabel:`Crop` tool and then click once to select the entire canvas. "
"Using this method you ensure that you don't inadvertently grab outside of "
"the visible canvas as part of the crop. You can then use the options below "
"to refine your crop. Press :kbd:`Enter` to finalize the crop action, or use "
"the :guilabel:`Crop` button in the tool options docker."
msgstr ""

#: ../../reference_manual/tools/crop.rst:20
msgid ""
"At its most basic, the crop tool allows you to size a rectangle around an "
"area and reduce your image or layer to only that content which is contained "
"within that area. There are several options which give a bit more "
"flexibility and precision."
msgstr ""

#: ../../reference_manual/tools/crop.rst:22
msgid ""
"The two numbers on the left are the exact horizontal position and vertical "
"position of the left and top of the cropping frame respectively. The numbers "
"are the right are from top to bottom: width, height, and aspect ratio. "
"Selecting the check boxes will keep any one of these can be locked to allow "
"you to manipulate the other two without losing the position or ratio of the "
"locked property."
msgstr ""

#: ../../reference_manual/tools/crop.rst:24
msgid "Center"
msgstr "中间对齐"

#: ../../reference_manual/tools/crop.rst:25
msgid "Keeps the crop area centered."
msgstr ""

#: ../../reference_manual/tools/crop.rst:26
msgid "Grow"
msgstr "增大"

#: ../../reference_manual/tools/crop.rst:27
msgid "Allows the crop area to expand beyond the image boundaries."
msgstr ""

#: ../../reference_manual/tools/crop.rst:28
msgid "Applies to"
msgstr ""

#: ../../reference_manual/tools/crop.rst:29
msgid ""
"Lets you apply the crop to the entire image or only to the active layer. "
"When you are ready, hit the :guilabel:`Crop` button and the crop will apply "
"to your image."
msgstr ""

#: ../../reference_manual/tools/crop.rst:31
msgid "Decoration"
msgstr ""

#: ../../reference_manual/tools/crop.rst:31
msgid ""
"Help you make a composition by showing you lines that divide up the screen. "
"You can for example show thirds here, so you can crop your image according "
"to the Rule of Thirds."
msgstr ""

#: ../../reference_manual/tools/crop.rst:34
msgid "Continuous Crop"
msgstr "连续裁切"

#: ../../reference_manual/tools/crop.rst:36
msgid ""
"If you crop an image, and try to start a new one directly afterwards, Krita "
"will attempt to recall the previous crop, so you can continue it. This is "
"the *continuous crop*. You can press :kbd:`Esc` to cancel this and crop anew."
msgstr ""
